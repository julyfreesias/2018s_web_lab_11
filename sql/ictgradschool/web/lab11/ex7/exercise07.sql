

 DROP TABLE IF EXISTS comments;

 CREATE TABLE comments (

   commentId INT NOT NULL AUTO_INCREMENT,
   articleId INT NOT NULL,
   comment VARCHAR(500),
   PRIMARY KEY (commentId),
   FOREIGN KEY (articleId) REFERENCES article (id)
 );

INSERT INTO comments(articleId, comment) VALUES
  (1, 'hello Jack!!'),
  (1,'This is Soo!');
-- Answers to Exercise 6 here

DROP TABLE IF EXISTS dbtest_tablesix;

CREATE TABLE dbtest_tablesix (

id VARCHAR (50) NOT NULL,
director VARCHAR  (20)NOT NULL,
movie VARCHAR  (20)NOT NULL,
chargeoutRate INT (50)NOT NULL,
name VARCHAR(50),
PRIMARY KEY(id),
FOREIGN KEY(name) REFERENCES  video_rental_statistics (name)
);

INSERT INTO dbtest_tablesix (id,director,movie,chargeoutRate,name) VALUES
('a', 'SK', 'moon', '2', 'Lorde'),
('b', 'Erns Rutherford', 'sun', '2', 'Kate Sheppard'),
('c', 'SK', 'ocean', '6', 'Ernest Rutherford'),
('d', 'bibi', 'universe', '4', 'Apirana Turupa Ngata')



-- Answers to Exercise 5 here

DROP TABLE IF EXISTS student;

CREATE TABLE IF NOT EXISTS student (

username VARCHAR(50) NOT NULL,
fname VARCHAR  (20),
lname VARCHAR (50),
email VARCHAR (100),
PRIMARY KEY(username)
);

INSERT INTO student VALUES  ('melon','Jack','Peterson','melon@gmail.com');
INSERT INTO student VALUES  ('orange','Peter', 'Peter','orange@gmail.com');
INSERT INTO student VALUES  ('apple','Pete', 'Peterpan','apple@gmail.com');
INSERT INTO student VALUES  ('pineapple','Yu-Cheng', 'Good','pineapple@gmail.com');

